.. PIController Device:

PIController
-------------

Introduction
============
| PIController is a motion controller Devices for controlling PI motors through sockets.
| Currently, two kinds of PI motors are supported: 
| - The E-754: High-Speed Single-Channel Digital Piezo Controller 
| - The C-884: Four Axis Motion Controller

| The PIController single Device Server concerns: 
| - The PIBox device for PI controllers configuration and socket connection 
| - The PIAxis device per axis to be controlled

+------------------------+-----------------------+
|               Supported Controller             |
+========================+=======================+
| .. image:: E-754.png   |                       |
|     :height: 200       | E-754                 |
|     :width: 300        |                       |
+------------------------+-----------------------+
| .. image:: C-884.png   |                       |
|     :height: 200       | C-884                 |
|     :width: 300        |                       |
+------------------------+-----------------------+

PIBox
============

PIBox Properties
````````````````````

====================================== =========================== ===================  ===========================================
Property name                          Default value               Type                 Description
====================================== =========================== ===================  ===========================================
IPAddress                              172.0.0.1                   String               The IP address of the controller hardware
TCPPort                                50000                       String               The socket Port number
SocketConfig                           blocking = true;            String               | The scocket configuation parameters
                                       keepalive = true;                                | - blocking: The Blocking mode parameter
                                       nodelay = true;                                  | - keepalive: The Keep Alive parameter
                                       sndtmo = 1000;                                   | - nodelay: The NoDelay parameter
                                       rcvtmo = 1000                                    | - sndtmo: The Sender Timeout parameter
                                                                                        | - rcvtmo: The Receiver Timeout parameter 
HistoricSocketMessagesConfig                                       String               | The configuation of allSocketMessages attribute
                                       max = 10;                                        | - max: Max number of items to display
                                       timpestamp = true;                               | - timpestamp: Activate/desactivate displaying timestamp
                                       format = %Y-%m-%dT%H:%M:%S                       | - format: Timpestamp format

====================================== =========================== ===================  ===========================================


PIBox Commands
````````````````````

======================= =============== ======================= =============================================================================
Command name            Arg. in         Arg. out                Description
======================= =============== ======================= =============================================================================
Init                    Void            Void                    Re-initialise the device 
State                   Void            Long                    Return the device state
Status                  Void            Long                    Return the device status
ExecLowLevelCommand     String          String                  Send low level command
======================= =============== ======================= =============================================================================


PIBox Attributes
````````````````````````````

=============================== ======================== ================== ===============================================
Attribute name                  Read/Write               Type               Description
=============================== ======================== ================== ===============================================
model                           R                        String             Model of the current Controller
allSocketMessages               R                        String []          List of exchanged requests through the Socket
=============================== ======================== ================== ===============================================

PIBox States
````````````````````````````

=============================== ==========================================
State name                      Description
=============================== ==========================================
INIT                            Initialization in progress
ON                              Connected to Server TCP/IP
FAULT                           Communication error occured
=============================== ==========================================

PIAxis
============

PIAxis Properties
````````````````````

====================================== ========================= ===================  ==================================================
Property name                          Default value             Type                 Description
====================================== ========================= ===================  ==================================================
AxisDefinition                         0                         ULong                Axis identification ID
AxisInitType                           FNL                       String               | Type of the reference move.
                                                                                      | Possible values
                                                                                      | - FNL: Moves the given axis to its negative physical limit
                                                                                      | - FPL: Moves the given axis to its positive physical limit
AxisInitPosition                       0                         Double               Axis position at the end of initialization process
AxisInitVelocity                       0.0                       Double               Axis velocity for initialization process             
====================================== ========================= ===================  ==================================================


PIAxis Commands
````````````````````

=========================== =============== ======================= ===========================================
Command name                Arg. in         Arg. out                Description
=========================== =============== ======================= ===========================================
Init                        Void            Void                    Re-initialise the device 
State                       Void            Long                    Return the device state
Status                      Void            Long                    Return the device status
On                          Void            Void                    Command to turn on the axis motor
Off                         Void            Void                    Command to turn off the axis motor
Stop                        Void            Void                    Command to stop the axe motion
Backward                    Void            Void                    | Used only for C884 Motor Controller. 
                                                                    | Command to move the axis to it's physical negative limit 
Forward                     Void            Void                    | Used only for C884 Motor Controller. 
                                                                    | Command to move the axis to it's physical negative limit
InitialiseReferencePosition Double          Void                    Initialise the reference position of the Axis
=========================== =============== ======================= ===========================================


PIAxis Attributes
````````````````````````````

=============================== ======================== ================== ===============================================
Attribute name                  Read/Write               Type               Description
=============================== ======================== ================== ===============================================
position                        R/W                      Double             Axis position, in user unit
velocity                        R/W                      Double             Axis velocity in closed-loop mode
acceleration                    R/W                      Double             | Used only for C884 Motor Controller.
                                                                            | Axis acceleration in closed-loop mode
deceleration                    R/W                      Double             | Used only for C884 Motor Controller.
                                                                            | Axis deceleration in closed-loop mode
backwardLimitSwitch             R                        Boolean            Is negative limit switch reached
forwardLimitSwitch              R                        Boolean            Is positive limit switch reached
=============================== ======================== ================== ===============================================

PIAxis States
````````````````````````````

=============================== ==========================================
State name                      Description
=============================== ==========================================
INIT                            The axis position needs to be referenced
OFF                             Current axis motor is off
ON                              Current axis motor is on
MOVING                          Current axis is moving
FAULT                           Error occured
=============================== ==========================================

Communication Protocol
========================

=============================== ==========================  =================================================================
Attribute/Command               Format                      Description         
=============================== ==========================  =================================================================
model                           \*IDN?                      Get Device Identification
position - Read                 POS? <AxisId>               Get Real Position
position - Write                MOV <AxisId> value          Set Target Position
velocity - Read                 VEL? <AxisId>               Get Closed-Loop Velocity
velocity - Write                VEL <AxisId> value          Get Closed-Loop Velocity
acceleration - Read             ACC? <AxisId>               Get Closed-Loop Acceleration
acceleration - Write            ACC <AxisId> value          Set Closed-Loop Acceleration
deceleration - Read             DEC? <AxisId>               Get Closed-Loop Deceleration
deceleration - Write            DEC <AxisId> value          Set Closed-Loop Deceleration
backwardLimitSwitch             SRG? <AxisId> 1             | Query Status Register Value
                                                            | the negative limit switch is the bit 0 of the register returned value
forwardardLimitSwitch           SRG? <AxisId> 1             | Query Status Register Value
                                                            | the positive limit switch is the bit 2 of the register returned value                                    
On                              SVO <AxisId> 1              Set Servo Mode
Off                             SVO <AxisId> 0              Set Servo Mode
Backward                        | C-844:                    |                                         
                                | - FNL <AxisId>            | Fast Reference Move To Negative Limit
                                | E-754:                    |                                        
                                | - MOV <AxisId> 0          | Set Target Position to 0
Forward                         FPL <AxisId> (C-844)        Fast Reference Move To Positive Limit
Stop                            HLT <AxisId>                Halt Motion Smoothly
is axis initialised             | C-844:                    | 
                                | - FRF?                    | Get Referencing Result
                                | E-754:                    | 
                                | - ATZ?                    | Get State Of Automatic Zero Point Calibration
InitialiseReferencePosition     | C-844:                    | 
                                | - VEL <AxisId> value      | 
                                | - RON <AxisId> 0          | Set Reference Mode to 0
                                | - FNL ou FPL              | 
                                | - FNL ou FPL              | 
                                | - RON <AxisId> 1          | Set Reference Mode to 1
                                | E-754:                    | 
                                | - VEL <AxisId> value      | 
                                | - ATZ <AxisId> NaN        | Set Automatic Zero Point Calibration
=============================== ==========================  =================================================================

