########################################################
#usage :
#python ServerSocketMock.py <-f config file> <-m host> <-p port1|...|portn> <-r rcv_eol> <-s send_eol> <-d delimiter>"
########################################################
#it is possible to access any of these global variables in each [section] other then [USER_VARIABES]
########################################################
#author      -> Arafat NOUREDDINE
#company     -> Synchrotron SOLEIL
#group       -> Informatique de Controle Acquisition
#version     -> version of the ServerSocketMock
#ThreadCount -> the numero of thread for each connected client
#command     -> string received from client
#config_file -> the config file defined by user (normally this one)
#host        -> the host address or name
#port        -> the port socket
#rcv_eol     -> the eol of received strings
#send_eol    -> the eol for sent strings
#delimiter   -> the delimiter to recognize command and choose wich [section] to use in this config file
########################################################


[USER_VARIABLES]
#this i a special section to declare user variables, the syntaxe  is xxx=yyy
model=(c)MOCK, SIMULATOR, 111111111, 1.0.0.0
servo=1
pos=10.000000
vel=5.000000
accel=2.000000
decel=1.000000
moving=0
ron=1
err=0
limit_switch=0x9004
is_initialized=1
[END]

[*IDN?]
my_var = globals()['read_vars']('model')
reply = str(my_var)
verbose = True
[END]

[ERR?]
my_var = globals()['read_vars']('err')
reply = str(my_var)
verbose = True
[END]

[SVO]
globals()['write_vars']('servo', command.split(" ")[2])
reply="????"
verbose = True
[END]

[SVO?]
my_var = globals()['read_vars']('servo')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[#5]
my_var = globals()['read_vars']('moving')
reply=str(my_var)
verbose=True
[END]

[FNL]
def move(a):
    return 1<<(a-1)
globals()['write_vars']('moving',  str(move(int(command.split(" ")[1]))))
globals()['write_vars']('pos', '0')
globals()['write_vars']('limit_switch', '0x9001')
globals()['write_vars']('is_initialized', '1')
def callback():
    globals()['write_vars']('moving', '0')
timer = threading.Timer(3.0, callback)
timer.start()
reply="????"
verbose = True
[END]

[FPL]
def move(a):
    return 1<<(a-1)
globals()['write_vars']('moving',  str(move(int(command.split(" ")[1]))))
globals()['write_vars']('pos', '10')
globals()['write_vars']('limit_switch', '0x9004')
globals()['write_vars']('is_initialized', '1')
def callback():
    globals()['write_vars']('moving','0')
timer = threading.Timer(3.0, callback)
timer.start()
reply="????"
verbose = True
[END]

[POS?]
my_var = globals()['read_vars']('pos')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[MOV]
def move(a):
    return 1<<(a-1)

def limit(pos):
    if float(pos) == 0:
        return '0x9001'
    elif float(pos) == 10:
        return '0x9004'
    return '0x9000'

globals()['write_vars']('moving',  str(move(int(command.split(" ")[1]))))
globals()['write_vars']('pos', command.split(" ")[2])
globals()['write_vars']('limit_switch', limit(globals()['read_vars']('pos')))

def callback():
    globals()['write_vars']('moving','0')
timer = threading.Timer(3.0, callback)
timer.start()
reply="????"
verbose = True
[END]

[RON]
globals()['write_vars']('ron', command.split(" ")[2])
reply="????"
verbose = True
[END]

[POS]
reply="????"
verbose = True
[END]

[VEL?]
my_var = globals()['read_vars']('vel')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[VEL]
globals()['write_vars']('vel', command.split(" ")[2])
reply="????"
verbose = True
[END]

[ACC?]
my_var = globals()['read_vars']('accel')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[ACC]
globals()['write_vars']('accel', command.split(" ")[2])
reply="????"
verbose = True
[END]

[DEC?]
my_var = globals()['read_vars']('decel')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[DEC]
globals()['write_vars']('decel', command.split(" ")[2])
reply="????"
verbose = True
[END]

[HLT]
globals()['write_vars']('moving','0')
reply="????"
verbose = True
[END]

[SRG?]
my_var = globals()['read_vars']('limit_switch')
reply = command.split(" ")[1] + str(" 1=") + str(my_var)
verbose = True
[END]

[FRF?]
my_var = globals()['read_vars']('is_initialized')
reply = command.split(" ")[1] + str("=") + str(my_var)
verbose = True
[END]

[ATZ?]
reply="????"
verbose = True
[END]
