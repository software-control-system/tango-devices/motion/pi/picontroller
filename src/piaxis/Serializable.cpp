/*************************************************************************/
/*! 
 *  \file   Serializable.cpp
 *  \brief  Implementation of Serializable methods.
 *	\author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include <tango.h>

#include "Serializable.h"
#include "BoxController.h"

namespace PIAxis_ns
{

yat::String Serializable::m_model = "Unkown";

void Serializable::check_command(const std::string& command, const std::string& origin)
{
    if( !has_command(command) )
    {
        std::string err = "Command '" + command + "' not handled for this device";
        Tango::Except::throw_exception("INTERNAL_ERROR",
                err.c_str(),
                origin.c_str());
    }
}

bool Serializable::has_command(const std::string& command)
{
    return PIBox_ns::BoxController::get_instance()->has_command(command);
}

std::string Serializable::termination_chars()
{
    return "\n";
}

// ---------------- Command ---------------- //

//----------------------------------------------------
// Command::pull_request()
// A command do not have a pull request
//----------------------------------------------------
yat::String Command::pull_request(int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "Command::pull_request()");
    return "";
}

//----------------------------------------------------
// Command::extract()
//----------------------------------------------------
void Command::extract(yat::String str, int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "Command::extract()");
}

// ---------------- Model ---------------- //

//----------------------------------------------------
// Model::pull_request()
// Command to get the model of the controller
//----------------------------------------------------
yat::String Model::pull_request(int axis_id)
{
    return "*IDN?";
}

//----------------------------------------------------
// Model::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Model::extract(yat::String str, int axis_id)
{
    size_t pos_ctrl_884 = str.find("C-884");
    if ( pos_ctrl_884 != std::string::npos)
    {
        m_value = str.substr(pos_ctrl_884, 5);
        return;
    }
	
    size_t pos_ctrl_754 = str.find("E-754");
    if(pos_ctrl_754 != std::string::npos)
    {
        m_value = str.substr(pos_ctrl_754, 5);
        return;
    }
	
    size_t pos_ctrl_simu = str.find("SIMULATOR");
	if(pos_ctrl_simu != std::string::npos )
	{
        m_value = "SIMULATOR";
        return;
	}
}

//----------------------------------------------------
// Model::push_request()
//----------------------------------------------------
yat::String Model::push_request(int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "Model::push_request()");
    return "";
}

//----------------------------------------------------
// Model::get_value()
//----------------------------------------------------
yat::String Model::get_value()
{
    return m_value;
}


// ---------------- On ---------------- //

//----------------------------------------------------
// On::pull_request()
// Command to get the servo mode
//----------------------------------------------------
yat::String On::pull_request(int axis_id)
{
    check_command("SVO?", "On::pull_request()");
    return "SVO? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// On::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void On::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<int>(str.substr(pos + 1, 1));
    }
}

//----------------------------------------------------
// On::push_request()
// Command to set the servo mode to 1
//----------------------------------------------------
yat::String On::push_request(int axis_id)
{
    check_command("SVO", "On::push_request()");
    return "SVO " +  yat::StringUtil::to_string(axis_id) + " 1";
}

bool On::has_pull_command()
{
    return has_command("SVO?");
}

bool On::has_push_command()
{
    return has_command("SVO");
}

//----------------------------------------------------
// On::get_value()
//----------------------------------------------------
int On::get_value()
{
    return m_value;
}

// ---------------- Off ---------------- //

//----------------------------------------------------
// Off::pull_request()
// Command to get the servo mode
//----------------------------------------------------
yat::String Off::pull_request(int axis_id)
{
    check_command("SVO?", "Off::pull_request()");
    return "SVO? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// Off::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Off::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<int>(str.substr(pos + 1, 1));
    }
}

//----------------------------------------------------
// Off::push_request()
// Command to set the servo mode to 0
//----------------------------------------------------
yat::String Off::push_request(int axis_id)
{
    check_command("SVO", "Off::push_request()");
    return "SVO " +  yat::StringUtil::to_string(axis_id) + " 0";
}

//----------------------------------------------------
// Off::get_value()
//----------------------------------------------------
int Off::get_value()
{
    return m_value;
}

bool Off::has_pull_command()
{
    return has_command("SVO?");
}

bool Off::has_push_command()
{
    return has_command("SVO");
}

// ---------------- IsMoving ---------------- //

//----------------------------------------------------
// IsMoving::pull_request()
// Command to get the moving state of an axis
//----------------------------------------------------
yat::String IsMoving::pull_request(int axis_id)
{
    if (m_model == "SIMULATOR")
    {
        return "#5"; //Command for the MockServer
    }
    else
    {
        yat::String cmd;
        cmd = 5;
        return cmd;
    }
}

//----------------------------------------------------
// IsMoving::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void IsMoving::extract(yat::String str, int axis_id)
{
    size_t state = yat::StringUtil::to_num<int>(str);
    m_value = 1 & (state >> (axis_id - 1));
}

//----------------------------------------------------
// IsMoving::push_request()
//----------------------------------------------------
yat::String IsMoving::push_request(int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "IsMoving::push_request()");
    return "";
}

std::string IsMoving::termination_chars()
{
    //No termination chars.
    return "";
}


//----------------------------------------------------
// IsMoving::get_value()
//----------------------------------------------------
int IsMoving::get_value()
{
    return m_value;
}

// ---------------- Position ---------------- //

//----------------------------------------------------
// Position::pull_request()
// Command to get the moving state of an axis
//----------------------------------------------------
yat::String Position::pull_request(int axis_id)
{
    check_command("POS?", "Position::pull_request()");
    return "POS? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// Position::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Position::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<double>(str.substr(pos + 1, str.size() - 2));
    }
}

//----------------------------------------------------
// Position::push_request()
//----------------------------------------------------
yat::String Position::push_request(int axis_id)
{
    check_command("MOV", "Position::push_request()");
    return "MOV " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_arg);
}

//----------------------------------------------------
// Position::get_value()
//----------------------------------------------------
double Position::get_value()
{
    return m_value;
}

//----------------------------------------------------
// Position::set_value()
//----------------------------------------------------
void Position::set_value(double arg)
{
    m_arg = arg;
}

bool Position::has_pull_command()
{
    return has_command("POS?");
}

bool Position::has_push_command()
{
    return has_command("MOV");
}

// ---------------- Velocity ---------------- //

//----------------------------------------------------
// Velocity::pull_request()
// Command to get the moving state of an axis
//----------------------------------------------------
yat::String Velocity::pull_request(int axis_id)
{
    check_command("VEL?", "Velocity::pull_request()");
    return "VEL? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// Velocity::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Velocity::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<double>(str.substr(pos + 1, str.size() - 2));
    }
}

//----------------------------------------------------
// Velocity::push_request()
//----------------------------------------------------
yat::String Velocity::push_request(int axis_id)
{
    check_command("VEL", "Velocity::push_request()");
    return "VEL " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_arg);
}

//----------------------------------------------------
// Velocity::get_value()
//----------------------------------------------------
double Velocity::get_value()
{
    return m_value;
}

//----------------------------------------------------
// Velocity::set_value()
//----------------------------------------------------
void Velocity::set_value(double arg)
{
    m_arg = arg;
}

bool Velocity::has_pull_command()
{
    return has_command("VEL?");
}

bool Velocity::has_push_command()
{
    return has_command("VEL");
}

// ---------------- Acceleration ---------------- //

//----------------------------------------------------
// Acceleration::pull_request()
// Command to get the acceleration of an axis
//----------------------------------------------------
yat::String Acceleration::pull_request(int axis_id)
{
    check_command("ACC?", "Acceleration::pull_request()");
    return "ACC? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// Acceleration::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Acceleration::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<double>(str.substr(pos + 1, str.size() - 2));
    }
}

//----------------------------------------------------
// Acceleration::push_request()
//----------------------------------------------------
yat::String Acceleration::push_request(int axis_id)
{
    check_command("ACC", "Acceleration::push_request()");
    return "ACC " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_arg);
}

//----------------------------------------------------
// Acceleration::get_value()
//----------------------------------------------------
double Acceleration::get_value()
{
    return m_value;
}

//----------------------------------------------------
// Acceleration::set_value()
//----------------------------------------------------
void Acceleration::set_value(double arg)
{
    m_arg = arg;
}

bool Acceleration::has_pull_command()
{
    return has_command("ACC?");
}

bool Acceleration::has_push_command()
{
    return has_command("ACC");
}

// ---------------- Deceleration ---------------- //

//----------------------------------------------------
// Deceleration::pull_request()
// Command to get the deceleration of an axis
//----------------------------------------------------
yat::String Deceleration::pull_request(int axis_id)
{
    check_command("DEC?", "Deceleration::pull_request()");
    return "DEC? " + yat::StringUtil::to_string(axis_id);
}

//----------------------------------------------------
// Deceleration::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void Deceleration::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<double>(str.substr(pos + 1, str.size() - 2));
    }
}

//----------------------------------------------------
// Deceleration::push_request()
//----------------------------------------------------
yat::String Deceleration::push_request(int axis_id)
{
    check_command("DEC", "Deceleration::push_request()");
    return "DEC " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_arg);
}

//----------------------------------------------------
// Deceleration::get_value()
//----------------------------------------------------
double Deceleration::get_value()
{
    return m_value;
}

//----------------------------------------------------
// Deceleration::set_value()
//----------------------------------------------------
void Deceleration::set_value(double arg)
{
    m_arg = arg;
}

bool Deceleration::has_pull_command()
{
    return has_command("DEC?");
}

bool Deceleration::has_push_command()
{
    return has_command("DEC");
}

// ---------------- Stop ---------------- //

//----------------------------------------------------
// Stop::push_request()
// Command to stop the axis motion
//----------------------------------------------------
yat::String Stop::push_request(int axis_id)
{
    check_command("HLT", "Stop::push_request()");
    return "HLT " +  yat::StringUtil::to_string(axis_id);
}

bool Stop::has_push_command()
{
    return has_command("HLT");
}

// ---------------- Forward ---------------- //

//----------------------------------------------------
// Forward::push_request()
// Command to move to positive limit
//----------------------------------------------------
yat::String Forward::push_request(int axis_id)
{
    check_command("FPL", "Forward::push_request()");
    yat::String return_str = "FPL " +  yat::StringUtil::to_string(axis_id);
    return return_str;
}

bool Forward::has_push_command()
{
    return has_command("FPL");
}

// ---------------- Backward ---------------- //

//----------------------------------------------------
// Backward::push_request()
// Command to move to negative limit
//----------------------------------------------------
yat::String Backward::push_request(int axis_id)
{
    yat::String return_str("");
    if (has_command("FNL"))
    {
        return_str = "FNL " +  yat::StringUtil::to_string(axis_id);
    }
    else if (has_command("MOV"))
    {
        return_str = "MOV " + yat::StringUtil::to_string(axis_id) + " 0";
    }
    else
    {
        Tango::Except::throw_exception("INTERNAL_ERROR",
                        "Command not handled for this device",
                        "Backward::push_request()");
    }
    
    return return_str;
}

bool Backward::has_push_command()
{
    return has_command("FNL") || has_command("MOV");
}

// ---------------- LimitSwitch ---------------- //

//----------------------------------------------------
// LimitSwitch::pull_request()
//----------------------------------------------------
yat::String LimitSwitch::pull_request(int axis_id)
{
    check_command("SRG?", "LimitSwitch::push_request()");
    return "SRG? " + yat::StringUtil::to_string(axis_id) + " 1";
}

//----------------------------------------------------
// LimitSwitch::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void LimitSwitch::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = std::strtol(str.substr(pos + 1, str.size() - 4).c_str(), NULL, 0);
    }
}

//----------------------------------------------------
// LimitSwitch::push_request()
//----------------------------------------------------
yat::String LimitSwitch::push_request(int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "LimitSwitch::push_request()");
    return "";
}

bool LimitSwitch::has_pull_command()
{
    return has_command("SRG?");
}

bool LimitSwitch::has_push_command()
{
    return false;
}

//----------------------------------------------------
// LimitSwitch::get_value()
//----------------------------------------------------
int LimitSwitch::get_value()
{
    return m_value;
}

// ---------------- Ron ---------------- //

//----------------------------------------------------
// Ron::push_request()
// Command to move to negative limit
//----------------------------------------------------
yat::String Ron::push_request(int axis_id)
{
    check_command("RON", "Ron::push_request()");
    return "RON " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_value);
}

//----------------------------------------------------
// Ron::set_value()
//----------------------------------------------------
void Ron::set_value(int arg)
{
    m_value = arg;
}

bool Ron::has_push_command()
{
    return has_command("RON");
}


// ---------------- Pos ---------------- //

//----------------------------------------------------
// Pos::push_request()
// Command to move to negative limit
//----------------------------------------------------
yat::String Pos::push_request(int axis_id)
{
    check_command("POS", "Pos::push_request()");
    return "POS " + yat::StringUtil::to_string(axis_id) + " " + yat::StringUtil::to_string(m_value);
}

//----------------------------------------------------
// Pos::set_value()
//----------------------------------------------------
void Pos::set_value(double arg)
{
    m_value = arg;
}

bool Pos::has_push_command()
{
    return has_command("POS");
}

// ---------------- IsInitialized ---------------- //

//----------------------------------------------------
// IsInitialized::pull_request()
// Command to know is the device is initiliazed
//----------------------------------------------------
yat::String IsInitialized::pull_request(int axis_id)
{
    yat::String return_str;
    if (has_command("FRF?"))
    {
        return_str = "FRF? " + yat::StringUtil::to_string(axis_id);
    }
    else if (has_command("ATZ?"))
    {
        return_str = "ATZ? " + yat::StringUtil::to_string(axis_id);
    }
    else
    {
        Tango::Except::throw_exception("INTERNAL_ERROR",
                        "Command not handled for this device",
                        "Backward::push_request()");
    }
    return return_str;
}

//----------------------------------------------------
// IsInitialized::extract()
// Extract the information from the response to the
// pull request
//----------------------------------------------------
void IsInitialized::extract(yat::String str, int axis_id)
{
    size_t pos = str.find("=");
    if (pos != std::string::npos)
    {
        m_value = yat::StringUtil::to_num<int>(str.substr(pos + 1, str.size() - 2));
    }
}

//----------------------------------------------------
// IsInitialized::push_request()
//----------------------------------------------------
yat::String IsInitialized::push_request(int axis_id)
{
    Tango::Except::throw_exception("INTERNAL_ERROR",
                        "This method is not available",
                        "IsInitialized::push_request()");
    return " ";
}

//----------------------------------------------------
// IsInitialized::get_value()
//----------------------------------------------------
bool IsInitialized::get_value()
{
    return m_value;
}

bool IsInitialized::has_pull_command()
{
    return has_command("FRF?") || has_command("ATZ?");
}

bool IsInitialized::has_push_command()
{
    return false;
}


// ---------------- AutoZero ---------------- //

//----------------------------------------------------
// AutoZero::push_request()
// Command to move to ZERO
//----------------------------------------------------
yat::String AutoZero::push_request(int axis_id)
{
    check_command("ATZ", "AutoZero::push_request()");
    return "ATZ " + yat::StringUtil::to_string(axis_id) + " NaN";
}

bool AutoZero::has_push_command()
{
    return has_command("ATZ");
}
} // namespace PIAxis_ns
