//************************************************************************//
//! 
// \file Serializer.h
// \author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
// \date 18 October 2022
// \brief class Serializer
// 
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//************************************************************************/

#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <yat/utils/String.h>

#include "Serializable.h"

namespace PIAxis_ns
{
//--------------------------------------------------------------------------------
//! \class Serializer
//! \brief class to abstract the communication between the device and the server
//--------------------------------------------------------------------------------
class Serializer
{
public:

    //! \brief Push from device a request that do not need a response
    //! \param serial: Serializable object
    static void push_to_server(Serializable* serial, int axis_id);

    //! \brief Push from device a request that need a response from the server
    //! \param serial: Serializable object
    static void pull_from_server(Serializable* serial, int axis_id);

    //! \brief Ask for the error code from the server and handle it
    static void error_handler(const std::string& request, const std::string& request_type);

};

} // namespace PIAxis_ns


#endif // SERIALIZER_H