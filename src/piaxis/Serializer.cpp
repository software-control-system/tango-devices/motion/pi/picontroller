/*************************************************************************/
/*! 
 *  \file   Serializer.cpp
 *  \brief  Implementation of Serializer methods.
 *	\author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include <tango.h>

#include "Serializer.h"
#include "SocketManager.h"

namespace PIAxis_ns
{

//----------------------------------------------------
// Serializer::push_to_server()
// Send a push request to the server
//----------------------------------------------------
void Serializer::push_to_server(Serializable* serial, int axis_id)
{
    yat::String request = serial->push_request(axis_id) + serial->termination_chars(); 
    PIBox_ns::SocketManager::get_instance()->write(request);
    error_handler(request, "push");
}

//----------------------------------------------------
// Serializer::pull_from_server()
// Send a pull request to the server
//----------------------------------------------------
void Serializer::pull_from_server(Serializable* serial, int axis_id)
{
    yat::String request = serial->pull_request(axis_id) + serial->termination_chars();
    yat::String response = PIBox_ns::SocketManager::get_instance()->write_read(request);
    error_handler(request, "pull");
    serial->extract(response, axis_id);
}

std::string trim(const std::string& str)
{
    size_t start = str.find_first_not_of(" \t\n\r\f\v");
    if (start == std::string::npos) {
        return "";
    }
    size_t end = str.find_last_not_of(" \t\n\r\f\v");
    return str.substr(start, end - start + 1);
}

//----------------------------------------------------
// Serializer::error_handler()
//----------------------------------------------------
void Serializer::error_handler(const std::string& request, const std::string& request_type)
{
    yat::String err = PIBox_ns::SocketManager::get_instance()->write_read("ERR?\n");
    if (err[0] != '0')
    {
        err = trim(err);
        yat::String desc = "Hardware error code for " + request_type +" request '"+ request.substr(0, request.length()-1) + "': " + err;
        try
        {
            int err_code = err.to_num<int>();
            if (err_code == 2)
            {
                desc += ": Unknown command";
            }
            else if (err_code == 7)
            {
                desc += ": Position out of limits";
            }
            else if (err_code == 8)
            {
                desc += ": Velocity  out of limits";
            }
            else if (err_code == 10)
            {
                desc += ": Controller was stopped by command";
            }
            else if (err_code == 17)
            {
                desc += ": Parameter out of range";
            }
        }
        catch( const yat::Exception& ex)
        {
            //Returned error is not an int. Nothing to do.
        }
        Tango::Except::throw_exception("HARDWARE_ERROR",
                        desc,
                        "Serializer::error_handler()");
    }
}

} // namespace PIAxis_ns
