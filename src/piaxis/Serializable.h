//************************************************************************
//! 
// \file Serializable.h
// \author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
// \date 18 October 2022
// \brief class Serializable
// 
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//************************************************************************/

#ifndef SERIALIZABLE_h
#define SERIALIZABLE_h

#include <stdlib.h>
#include <yat/utils/String.h>


namespace PIAxis_ns
{
//--------------------------------------------------------------------------------
//! \class Serializable << Abstract >>
//! \brief class to abstract the formatting of a request send to the server
//--------------------------------------------------------------------------------
class Serializable
{
public:

    //! \brief Create a request that need a response from the Hardware
    virtual yat::String pull_request(int axis_id) = 0;

    //! \brief Extract the needed information from a yat::String
    //! \param str: yat::String that contains the informations we need
    virtual void extract(yat::String str, int axis_id) = 0;

    //! \brief  Create a request that do not need a response from the Hardware
    virtual yat::String push_request(int axis_id) = 0;

    //! \brief  Give the termination characters for this request. Usually '\n' except for single char request.
    virtual std::string termination_chars();

    //! 
    virtual bool has_pull_command() = 0;

    //! 
    virtual bool has_push_command() = 0;

    //! \brief m_model: model of the controller
    static yat::String m_model;

protected:
    void check_command(const std::string& command, const std::string& origin);
    bool has_command(const std::string& command);
};

class Command : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    bool has_pull_command()
    {
        return false;
    }
    void extract(yat::String str, int axis_id);
};

class Model : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    yat::String get_value();
    bool has_pull_command()
    {
        return true;
    }
    bool has_push_command()
    {
        return false;
    }

private:
    yat::String m_value;
};

class On : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    int get_value();
    bool has_pull_command();
    bool has_push_command();

private:
    int m_value;
};

class Off : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    int get_value();
    bool has_pull_command();
    bool has_push_command();

private:
    int m_value;
};

class IsMoving : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    std::string termination_chars();
    int get_value();
    bool has_pull_command()
    {
        return true;
    }
    bool has_push_command()
    {
        return false;
    }

private:
    int m_value;
};

class Position : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    double get_value();
    void set_value(double arg);
    bool has_pull_command();
    bool has_push_command();

private:
    double m_value;
    double m_arg;
};

class Velocity : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    double get_value();
    void set_value(double arg);
    bool has_pull_command();
    bool has_push_command();

private:
    double m_value;
    double m_arg;
};

class Acceleration : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    double get_value();
    void set_value(double arg);
    bool has_pull_command();
    bool has_push_command();

private:
    double m_value;
    double m_arg;
};

class Deceleration : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    double get_value();
    void set_value(double arg);
    bool has_pull_command();
    bool has_push_command();

private:
    double m_value;
    double m_arg;
};

class Stop : public Command
{
    public:
    yat::String push_request(int axis_id);
    bool has_push_command();
};

class Forward : public Command
{
    public:
    yat::String push_request(int axis_id);    
    bool has_push_command();
};

class Backward : public Command
{
    public:
    yat::String push_request(int axis_id);
    bool has_push_command();
};

class Ron : public Command
{
public:
    yat::String push_request(int axis_id);
    void set_value(int arg);
    bool has_push_command();

private:
    int m_value;
};

class Pos : public Command
{
public:
    yat::String push_request(int axis_id);
    void set_value(double arg);
    bool has_push_command();

private:
    double m_value;
};

class LimitSwitch : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    int get_value();
    bool has_pull_command();
    bool has_push_command();

private:
    int m_value;
};

class IsInitialized : public Serializable
{
public:
    yat::String pull_request(int axis_id);
    void extract(yat::String str, int axis_id);
    yat::String push_request(int axis_id);
    bool get_value();
    bool has_pull_command();
    bool has_push_command();

private:
    bool m_value;
};

class AutoZero : public Command
{
public:
    yat::String push_request(int axis_id);
    bool has_push_command();
    
};

} // namespace PIAxis_ns


#endif // SERIALIZABLE_h
