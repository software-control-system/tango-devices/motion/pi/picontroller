/*************************************************************************/
/*! 
 *  \file   AxisController.cpp
 *  \brief  Implementation of AxisController methods.
 *	\author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include <yat/utils/Logging.h>
#include <yat/threading/Thread.h>

#include "AxisController.h"
#include "Serializer.h"
#include "BoxController.h"

namespace PIAxis_ns
{

//----------------------------------------------------
// AxisController::AxisController(long axis_id)
//----------------------------------------------------
AxisController::AxisController(Tango::DeviceImpl* dev, long axis_id, yat::String init_type,
                                double init_position, double init_velocity)
    :yat4tango::DeviceTask(dev),
    m_axis_id(axis_id),
    m_init_type(init_type),
    m_init_position(init_position),
    m_init_velocity(init_velocity)
{
    INFO_STREAM << "AxisController::AxisController(m_axis_id = " << m_axis_id << " )."<< std::endl;
    enable_timeout_msg(false);
    enable_periodic_msg(false);
}

//----------------------------------------------------
// AxisController::~AxisController()
//----------------------------------------------------
AxisController::~AxisController()
{
    INFO_STREAM << "AxisController::~AxisController()."<< std::endl;
    
}

//----------------------------------------------------
// AxisController::get_model()
// Return the controller's model
//----------------------------------------------------
std::string AxisController::get_model()
{
    yat::Message* msg = yat::Message::allocate(CTRL_MODEL_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    Serializable::m_model = m_model.get_value();
    return Serializable::m_model;
}

//----------------------------------------------------
// AxisController::compute_state_status()
// Compute state and status on the device PIAxis
//----------------------------------------------------
void AxisController::compute_state_status()
{
    try
    {
        if(m_state != Tango::FAULT && m_state != Tango::INIT)
        {
            Serializer::pull_from_server(&m_on, m_axis_id);
            if (m_on.get_value() == 1)
            {
                set_state(Tango::STANDBY);
                set_status("Motor on");
                Serializer::pull_from_server(&m_is_moving, m_axis_id);
                if (m_is_moving.get_value() == 1)
                {
                    set_state(Tango::MOVING);
                    set_status("Moving");
                    return;
                }
            }
            else
            {
                set_state(Tango::OFF);
                set_status("Motor off");
            }
        }
    }
    catch(Tango::DevFailed& df)
    {
        tango_error_handler(df);
    }
    catch(const yat::Exception& e)
    {
        yat_error_handler(e,"Exception from - AxisController::compute_state_status() :\n");
    }
}

void AxisController::is_axis_initialized()
{
    try
    {
        Serializer::pull_from_server(&m_is_initialized, m_axis_id);
        if (m_is_initialized.get_value() == 0)
        {
            set_state(Tango::INIT);
            set_status("Axis need to be initialized");
        }
    }
    catch(Tango::DevFailed& df)
    {
        tango_error_handler(df);
    }
    catch(const yat::Exception& e)
    {
        yat_error_handler(e,"Exception from - AxisController::compute_state_status() :\n");
    }
}

//----------------------------------------------------
// AxisController::on()
// Turn the motor on
//----------------------------------------------------
void AxisController::on()
{
    yat::Message* msg = yat::Message::allocate(CTRL_ON_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::off()
// Turn the motor off
//----------------------------------------------------
void AxisController::off()
{
    yat::Message* msg = yat::Message::allocate(CTRL_OFF_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::read_position()
//----------------------------------------------------
double AxisController::read_position()
{
    yat::Message* msg = yat::Message::allocate(CTRL_GETPOSITION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return m_position.get_value();
}

//----------------------------------------------------
// AxisController::write_position()
//----------------------------------------------------
void AxisController::write_position(double arg)
{
    m_position.set_value(arg);
    yat::Message* msg = yat::Message::allocate(CTRL_SETPOSITION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::read_velocity()
//----------------------------------------------------
double AxisController::read_velocity()
{
    yat::Message* msg = yat::Message::allocate(CTRL_GETVELOCITY_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return m_velocity.get_value();
}

//----------------------------------------------------
// AxisController::write_velocity()
//----------------------------------------------------
void AxisController::write_velocity(double arg)
{
    m_velocity.set_value(arg);
    yat::Message* msg = yat::Message::allocate(CTRL_SETVELOCITY_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::read_acceleration()
//----------------------------------------------------
double AxisController::read_acceleration()
{
    yat::Message* msg = yat::Message::allocate(CTRL_GETACCELERATION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return m_acceleration.get_value();
}

//----------------------------------------------------
// AxisController::write_acceleration()
//----------------------------------------------------
void AxisController::write_acceleration(double arg)
{
    m_acceleration.set_value(arg);
    yat::Message* msg = yat::Message::allocate(CTRL_SETACCELERATION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::read_deceleration()
//----------------------------------------------------
double AxisController::read_deceleration()
{
    yat::Message* msg = yat::Message::allocate(CTRL_GETDECELERATION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return m_deceleration.get_value();
}

//----------------------------------------------------
// AxisController::write_deceleration()
//----------------------------------------------------
void AxisController::write_deceleration(double arg)
{
    m_deceleration.set_value(arg);
    yat::Message* msg = yat::Message::allocate(CTRL_SETDECELERATION_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

bool AxisController::has_read_velocity()
{
    return m_velocity.has_pull_command();
}

bool AxisController::has_write_velocity()
{
    return m_velocity.has_pull_command();
}

bool AxisController::has_read_acceleration()
{
    return m_acceleration.has_pull_command();
}

bool AxisController::has_write_acceleration()
{
    return m_acceleration.has_pull_command();
}

bool AxisController::has_read_deceleration()
{
    return m_deceleration.has_pull_command();
}

bool AxisController::has_write_deceleration()
{
    return m_deceleration.has_push_command();
}

//----------------------------------------------------
// AxisController:set_state()
//----------------------------------------------------
void AxisController::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_status_lock); 
    m_state = state;
}

//----------------------------------------------------
// AxisController::get_state()
//----------------------------------------------------
Tango::DevState AxisController::get_state()
{
    yat::MutexLock scoped_lock(m_state_status_lock); 
    return m_state;
}

//----------------------------------------------------
// AxisController::set_status()
//----------------------------------------------------
void AxisController::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_status_lock); 
    m_status.str("");
	m_status << status.c_str();
}

//----------------------------------------------------
// AxisController::get_status()
//----------------------------------------------------
std::string AxisController::get_status()
{
    yat::MutexLock scoped_lock(m_state_status_lock); 
    return m_status.str();
}

//----------------------------------------------------
// AxisController::get_axis_id()
//----------------------------------------------------
double AxisController::get_axis_id() const
{
    return m_axis_id;
}

//----------------------------------------------------
// AxisController::stop()
//----------------------------------------------------

void AxisController::stop()
{
    yat::Message* msg = yat::Message::allocate(CTRL_STOP_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::backward()
//----------------------------------------------------

void AxisController::backward()
{
    yat::Message* msg = yat::Message::allocate(CTRL_BACKWARD_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::forward()
//----------------------------------------------------

void AxisController::forward()
{
    yat::Message* msg = yat::Message::allocate(CTRL_FORWARD_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
}

//----------------------------------------------------
// AxisController::backward_limit()
//----------------------------------------------------

bool AxisController::backward_limit()
{
    yat::Message* msg = yat::Message::allocate(CTRL_NLIMIT_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return m_limit_switch.get_value() & 1;
}

//----------------------------------------------------
// AxisController::forward_limit()
//----------------------------------------------------

bool AxisController::forward_limit()
{
    yat::Message* msg = yat::Message::allocate(CTRL_PLIMIT_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000);
    return (m_limit_switch.get_value() >> 2) & 1;
}

//----------------------------------------------------
// AxisController::auto_zero()
//----------------------------------------------------

void AxisController::auto_zero()
{
    yat::Message* msg = yat::Message::allocate(CTRL_AUTO_ZERO_MSG, DEFAULT_MSG_PRIORITY, true);
    wait_msg_handled(msg, 5000); 
}


//----------------------------------------------------
// AxisController::initialise_reference_position()
//----------------------------------------------------

void AxisController::initialise_reference_position()
{
    m_velocity.set_value(m_init_velocity);
    yat::Message* msg = yat::Message::allocate(CTRL_SETVELOCITY_MSG, DEFAULT_MSG_PRIORITY, true);
    post(msg);
    if (PIBox_ns::BoxController::get_instance()->has_command("ATZ?"))
    {
        msg = yat::Message::allocate(CTRL_AUTO_ZERO_MSG, DEFAULT_MSG_PRIORITY, true);
        post(msg);
    }
    else
    {
        Serializer::pull_from_server(&m_on, m_axis_id);
        if (m_on.get_value() == 0)
        {
            Serializer::push_to_server(&m_on, m_axis_id);
        }
        msg = yat::Message::allocate(CTRL_RON_MSG, DEFAULT_MSG_PRIORITY, true);
        msg->attach_data<bool>(false);
        post(msg);
        if (m_init_type == "FNL")
        {
            msg = yat::Message::allocate(CTRL_BACKWARD_MSG, DEFAULT_MSG_PRIORITY, true);
            post(msg);
        }
        else if (m_init_type == "FPL")
        {
            msg = yat::Message::allocate(CTRL_FORWARD_MSG, DEFAULT_MSG_PRIORITY, true);
            post(msg);
        }
        //the FNL or FPL commands will reset the position value to the value of the limits, i.e. 0 or the max limit
        //Hence, we decided to not use the POS command.
        //If someday we decide to handle it, we will have to wait until the end of the motion of FNL/FPL before sending this command
        /*
        m_pos.set_value(m_init_position);
        msg = yat::Message::allocate(CTRL_POS_MSG, DEFAULT_MSG_PRIORITY, true);
        post(msg);
        */
        msg = yat::Message::allocate(CTRL_RON_MSG, DEFAULT_MSG_PRIORITY, true);
        msg->attach_data<bool>(true);
        post(msg);
    }
}
//----------------------------------------------------
// AxisController::process_message()
//----------------------------------------------------
void AxisController::process_message(yat::Message& msg)
{
    try
    {
        switch(msg.type())
        {
                //-----------------------------------------------------
            case yat::TASK_INIT:
            {
                INFO_STREAM << "-> AxisController::TASK_INIT" << std::endl;
                enable_periodic_msg(true);                
                set_state(Tango::OFF);
                set_status("NOT CONNECTED");
            }
                break;
                //-----------------------------------------------------
            case yat::TASK_EXIT:
            {
                INFO_STREAM << "-> AxisController::TASK_EXIT" << std::endl;
            }
                break;
                //-----------------------------------------------------
            case yat::TASK_TIMEOUT:
            {
                //INFO_STREAM << "-> AxisController::TASK_TIMEOUT" << std::endl;
            }
                break;
                //-----------------------------------------------------

            case yat::TASK_PERIODIC:
            {
               // DEBUG_STREAM << "-> AxisController::TASK_PERIODIC" << std::endl;
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_ON_MSG:
            {
                //DEBUG_STREAM << "-> AxisController::TASK_CTRL_ON_MSG" << std::endl;
                set_state(Tango::STANDBY);
                Serializer::push_to_server(&m_on, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_OFF_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_OFF_MSG" << std::endl;
                set_state(Tango::OFF);
                Serializer::push_to_server(&m_off, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_MODEL_MSG:
            {
                Serializer::pull_from_server(&m_model, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_SETPOSITION_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_SETPOSITION_MSG" << std::endl;
                set_state(Tango::MOVING);
                Serializer::push_to_server(&m_position, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_GETPOSITION_MSG:
            {
                Serializer::pull_from_server(&m_position, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_SETVELOCITY_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_SETVELOCITY_MSG" << std::endl;
                set_state(Tango::STANDBY);
                Serializer::push_to_server(&m_velocity, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_GETVELOCITY_MSG:
            {
                Serializer::pull_from_server(&m_velocity, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_SETACCELERATION_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_SETACCELERATION_MSG" << std::endl;
                set_state(Tango::STANDBY);
                Serializer::push_to_server(&m_acceleration, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_GETACCELERATION_MSG:
            {
                Serializer::pull_from_server(&m_acceleration, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_SETDECELERATION_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_SETDECELERATION_MSG" << std::endl;
                set_state(Tango::STANDBY);
                Serializer::push_to_server(&m_deceleration, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_GETDECELERATION_MSG:
            {
                Serializer::pull_from_server(&m_deceleration, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_NLIMIT_MSG:
            {
                Serializer::pull_from_server(&m_limit_switch, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_PLIMIT_MSG:
            {
                Serializer::pull_from_server(&m_limit_switch, m_axis_id);
            }
                break;
                //-----------------------------------------------------
 
            case CTRL_STOP_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_STOP_MSG" << std::endl;
                set_state(Tango::STANDBY);
                Serializer::push_to_server(&m_stop, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_BACKWARD_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_BACKWARD_MSG" << std::endl;
                set_state(Tango::MOVING);
                Serializer::push_to_server(&m_backward, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_FORWARD_MSG:
            {
                DEBUG_STREAM << "-> AxisController::TASK_CTRL_FORWARD_MSG" << std::endl;
                set_state(Tango::MOVING);
                Serializer::push_to_server(&m_forward, m_axis_id);
            }
                break;
                //-----------------------------------------------------

            case CTRL_RON_MSG:
            {
                m_ron.set_value(msg.get_data<bool>());
                Serializer::push_to_server(&m_ron, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_POS_MSG:
            {
                m_pos.set_value(m_init_position);
                Serializer::push_to_server(&m_pos, m_axis_id);
            }
                break;
                //-----------------------------------------------------
            
            case CTRL_AUTO_ZERO_MSG:
            {
                set_state(Tango::MOVING);
                Serializer::push_to_server(&m_auto_zero, m_axis_id);
            }
                break;
                //-----------------------------------------------------

 
        }
    }
    catch(Tango::DevFailed& df)
    {
        tango_error_handler(df);
    }
    catch(const yat::Exception& e)
    {
        yat_error_handler(e,"Exception from - AxisController::process_message() :\n");
    }
}

//----------------------------------------------------
// AxisController::tango_error_handler()
//----------------------------------------------------
void AxisController::tango_error_handler(Tango::DevFailed& df)
{  
    set_state(Tango::FAULT);
    std::string msg(df.errors[0].desc);
    set_status(msg);
    ERROR_STREAM << msg << std::endl;
}

//----------------------------------------------------
// AxisController::yat_error_handler()
//----------------------------------------------------
void AxisController::yat_error_handler(const yat::Exception& e,string error_display)
{    
    e.dump();
    set_state(Tango::FAULT);
    set_status(e.errors[0].desc);
    std::stringstream  error_msg("");
    error_msg << "Origin\t: " << e.errors[0].origin << std::endl;
    error_msg << "Desc\t: " << e.errors[0].desc << std::endl;
    error_msg << "Reason\t: " << e.errors[0].reason << std::endl;
    ERROR_STREAM << error_display << "\n"
                << error_msg.str() << std::endl;
}


} // namespace PIAxis
