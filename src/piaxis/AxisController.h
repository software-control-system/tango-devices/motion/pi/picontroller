//*************************************************************************
//! 
// \file AxisController.h
// \author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
// \date 18 October 2022
// \brief class AxisController
// 
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//*************************************************************************/

#ifndef AXIS_CONTROLLER_H
#define AXIS_CONTOLLER_H

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include <yat/utils/String.h>
#include <yat/threading/Mutex.h>

#include "Serializable.h"

const size_t CTRL_ON_MSG                = yat::FIRST_USER_MSG + 100;
const size_t CTRL_OFF_MSG               = yat::FIRST_USER_MSG + 110;
const size_t CTRL_MODEL_MSG             = yat::FIRST_USER_MSG + 120;
const size_t CTRL_SETPOSITION_MSG       = yat::FIRST_USER_MSG + 130;
const size_t CTRL_GETPOSITION_MSG       = yat::FIRST_USER_MSG + 140;
const size_t CTRL_SETVELOCITY_MSG       = yat::FIRST_USER_MSG + 150;
const size_t CTRL_GETVELOCITY_MSG       = yat::FIRST_USER_MSG + 160;
const size_t CTRL_SETACCELERATION_MSG   = yat::FIRST_USER_MSG + 170;
const size_t CTRL_GETACCELERATION_MSG   = yat::FIRST_USER_MSG + 180;
const size_t CTRL_SETDECELERATION_MSG   = yat::FIRST_USER_MSG + 190;
const size_t CTRL_GETDECELERATION_MSG   = yat::FIRST_USER_MSG + 200;
const size_t CTRL_STOP_MSG              = yat::FIRST_USER_MSG + 210;
const size_t CTRL_BACKWARD_MSG          = yat::FIRST_USER_MSG + 220;
const size_t CTRL_FORWARD_MSG           = yat::FIRST_USER_MSG + 230;
const size_t CTRL_NLIMIT_MSG            = yat::FIRST_USER_MSG + 240;
const size_t CTRL_PLIMIT_MSG            = yat::FIRST_USER_MSG + 250;
const size_t CTRL_INIT_REFPOS_MSG       = yat::FIRST_USER_MSG + 260;
const size_t CTRL_RON_MSG               = yat::FIRST_USER_MSG + 270;
const size_t CTRL_POS_MSG               = yat::FIRST_USER_MSG + 280;
const size_t CTRL_AUTO_ZERO_MSG         = yat::FIRST_USER_MSG + 290;

namespace PIAxis_ns
{
//--------------------------------------------------------------------------------
//! \class BoxController
//! \brief class Controller
//--------------------------------------------------------------------------------
class AxisController : public yat4tango::DeviceTask
{

public:
    
    //! \brief Constructor
    AxisController(Tango::DeviceImpl* dev, long axis_id, yat::String init_type,
                                double init_position, double init_velocity);
    
    //! \brief Destructor
    ~AxisController();

    //! \brief Get the Hardware Controller model
    std::string get_model();

    //! \brief Set state and status accordingly
    void is_axis_initialized();
    
    //! \brief Turn motor on
    void on();

    //! \brief Turn motor off
    void off();

    //! \brief compute state and status of the PIAxis controller
    void compute_state_status();

    //! \brief get internal state of PIAxis
    Tango::DevState get_state();

    //! \brief set internal state of PIAxis
    void set_state(Tango::DevState state);
    
    //! \brief get internal status of PIAxis
    std::string get_status();

    //! \brief set internal status of PIAxis
    void set_status(const std::string& status);

    //! \brief get current axis id
    double get_axis_id() const;

    //! \brief read the value of the position
    double read_position();

    //! \brief set the position
    void write_position(double arg);

    //! \brief read the value of the velocity
    double read_velocity();

    //! \brief set the velocity
    void write_velocity(double arg);

    //! \brief read the value of the acceleration
    double read_acceleration();

    //! \brief set the acceleration
    void write_acceleration(double arg);

    //! \brief read the value of the acceleration
    double read_deceleration();

    //! \brief set the acceleration
    void write_deceleration(double arg);

    //! \brief read the value of the velocity
    bool has_read_velocity();

    //! \brief set the velocity
    bool has_write_velocity();

    //! \brief read the value of the acceleration
    bool has_read_acceleration();

    //! \brief set the acceleration
    bool has_write_acceleration();

    //! \brief read the value of the acceleration
    bool has_read_deceleration();

    //! \brief set the acceleration
    bool has_write_deceleration();

    //! \brief stop the axis motion
    void stop();    

    //! \brief  move to backward limit
    void backward();    

    //! \brief move to forward limit
    void forward();         

    //! \brief detect if motor reached backward limit
    bool backward_limit();

    //! \brief detect if motor reached forward limit
    bool forward_limit();

    //! \brief initialise the reference position point
    void initialise_reference_position();

    //! \brief process messages task
    void process_message(yat::Message& msg);

    //! \brief initialise the reference position point
    void auto_zero();

    //! \brief error handler for tango
    void tango_error_handler(Tango::DevFailed& df);

    //! \brief error handler for yat
    void yat_error_handler(const yat::Exception& e,string error_display);



private:
    //! \brief m_axis_id: Axis id
    long m_axis_id;
    
    //! \brief m_model: Serializable for the model
    Model m_model;
    
    //! \brief m_on: Serializable to turn the motor on and get the moving state
    On m_on;
    
    //! \brief m_off: Serializable to turn the motor off and get the moving state
    Off m_off;
    
    //! \brief m_is_moving: Serializable to know the moving state of the axe
    IsMoving m_is_moving;

    //! \brief m_position
    Position m_position;

    //! \brief m_pos
    Pos m_pos;

    //! \brief m_velocity
    Velocity m_velocity;

    //! \brief m_acceleration;
    Acceleration m_acceleration;

    //! \brief m_deceleration;
    Deceleration m_deceleration;

    //! \brief m_stop: Stop the motion of the axis
    Stop m_stop;

    //! \brief m_backward: move to backward limit
    Backward m_backward;

    //! \brief m_forward: move to forward limit
    Forward m_forward;
    
    //! \brief m_limit_switch
    LimitSwitch m_limit_switch;

    //! \brief m_ron
    Ron m_ron;

    //! \brief m_init_type
    yat::String m_init_type;

    //! \brief m_init_position
    double m_init_position;

    //! \brief m_init_velocity
    double m_init_velocity;

    //! \brief m_is_initialized
    IsInitialized m_is_initialized;
    
    //! \brief m_autozero
    AutoZero m_auto_zero;

    //! \brief m_state: internal state of PIBox
    Tango::DevState m_state;
    
    //! \brief m_status: internal status of PIBox
    std::stringstream m_status;

    //! \brief m_state_lock: Mutex used to protect state and status access
    yat::Mutex m_state_status_lock;

};

} // namespace PIAxis_ns


#endif // AXIS_CONTOLLER_H