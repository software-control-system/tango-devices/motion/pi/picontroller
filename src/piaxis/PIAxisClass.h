/*----- PROTECTED REGION ID(PIAxisClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        PIAxisClass.h
//
// description : Include for the PIAxis root class.
//               This class is the singleton class for
//                the PIAxis device class.
//               It contains all properties and methods which the 
//               PIAxis requires only once e.g. the commands.
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef PIAxisClass_H
#define PIAxisClass_H

#include <tango.h>
#include <PIAxis.h>


/*----- PROTECTED REGION END -----*/	//	PIAxisClass.h


namespace PIAxis_ns
{
/*----- PROTECTED REGION ID(PIAxisClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	PIAxisClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute position class definition
class positionAttrib: public Tango::Attr
{
public:
	positionAttrib():Attr("position",
			Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~positionAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_position(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<PIAxis *>(dev))->write_position(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_position_allowed(ty);}
};

//	Attribute velocity class definition
class velocityAttrib: public Tango::Attr
{
public:
	velocityAttrib():Attr("velocity",
			Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~velocityAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_velocity(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<PIAxis *>(dev))->write_velocity(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_velocity_allowed(ty);}
};

//	Attribute acceleration class definition
class accelerationAttrib: public Tango::Attr
{
public:
	accelerationAttrib():Attr("acceleration",
			Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~accelerationAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_acceleration(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<PIAxis *>(dev))->write_acceleration(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_acceleration_allowed(ty);}
};

//	Attribute deceleration class definition
class decelerationAttrib: public Tango::Attr
{
public:
	decelerationAttrib():Attr("deceleration",
			Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~decelerationAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_deceleration(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<PIAxis *>(dev))->write_deceleration(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_deceleration_allowed(ty);}
};

//	Attribute backwardLimitSwitch class definition
class backwardLimitSwitchAttrib: public Tango::Attr
{
public:
	backwardLimitSwitchAttrib():Attr("backwardLimitSwitch",
			Tango::DEV_BOOLEAN, Tango::READ) {};
	~backwardLimitSwitchAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_backwardLimitSwitch(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_backwardLimitSwitch_allowed(ty);}
};

//	Attribute forwardLimitSwitch class definition
class forwardLimitSwitchAttrib: public Tango::Attr
{
public:
	forwardLimitSwitchAttrib():Attr("forwardLimitSwitch",
			Tango::DEV_BOOLEAN, Tango::READ) {};
	~forwardLimitSwitchAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<PIAxis *>(dev))->read_forwardLimitSwitch(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<PIAxis *>(dev))->is_forwardLimitSwitch_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command On class definition
class OnClass : public Tango::Command
{
public:
	OnClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	OnClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~OnClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_On_allowed(any);}
};

//	Command Off class definition
class OffClass : public Tango::Command
{
public:
	OffClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	OffClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~OffClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_Off_allowed(any);}
};

//	Command Stop class definition
class StopClass : public Tango::Command
{
public:
	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_Stop_allowed(any);}
};

//	Command Backward class definition
class BackwardClass : public Tango::Command
{
public:
	BackwardClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	BackwardClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~BackwardClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_Backward_allowed(any);}
};

//	Command Forward class definition
class ForwardClass : public Tango::Command
{
public:
	ForwardClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ForwardClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ForwardClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_Forward_allowed(any);}
};

//	Command InitialiseReferencePosition class definition
class InitialiseReferencePositionClass : public Tango::Command
{
public:
	InitialiseReferencePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	InitialiseReferencePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~InitialiseReferencePositionClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIAxis *>(dev))->is_InitialiseReferencePosition_allowed(any);}
};


/**
 *	The PIAxisClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  PIAxisClass : public Tango::DeviceClass
#else
class PIAxisClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(PIAxisClass::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	PIAxisClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static PIAxisClass *init(const char *);
		static PIAxisClass *instance();
		~PIAxisClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		PIAxisClass(string &);
		static PIAxisClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	PIAxis_H
