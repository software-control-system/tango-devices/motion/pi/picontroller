/*----- PROTECTED REGION ID(PIBoxStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        PIBoxStateMachine.cpp
//
// description : State machine file for the PIBox class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <PIBox.h>

/*----- PROTECTED REGION END -----*/	//	PIBox::PIBoxStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================
//  ON      |  Connected to Server TCP/IP
//  FAULT   |  Communication error occured


namespace PIBox_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : PIBox::is_model_allowed()
 *	Description : Execution allowed for model attribute
 */
//--------------------------------------------------------
bool PIBox::is_model_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(PIBox::modelStateAllowed_READ) ENABLED START -----*/
			if (get_state()==Tango::FAULT && is_device_initialized())
			{	
				return true;
			}
	/*----- PROTECTED REGION END -----*/	//	PIBox::modelStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : PIBox::is_allSocketMessages_allowed()
 *	Description : Execution allowed for allSocketMessages attribute
 */
//--------------------------------------------------------
bool PIBox::is_allSocketMessages_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for allSocketMessages attribute in read access.
	/*----- PROTECTED REGION ID(PIBox::allSocketMessagesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PIBox::allSocketMessagesStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : PIBox::is_availableCommands_allowed()
 *	Description : Execution allowed for availableCommands attribute
 */
//--------------------------------------------------------
bool PIBox::is_availableCommands_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for availableCommands attribute in read access.
	/*----- PROTECTED REGION ID(PIBox::availableCommandsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PIBox::availableCommandsStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : PIBox::is_ExecLowLevelCommand_allowed()
 *	Description : Execution allowed for ExecLowLevelCommand attribute
 */
//--------------------------------------------------------
bool PIBox::is_ExecLowLevelCommand_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(PIBox::ExecLowLevelCommandStateAllowed) ENABLED START -----*/
		if (get_state()==Tango::FAULT && is_device_initialized())
		{	
			return true;
		}
	/*----- PROTECTED REGION END -----*/	//	PIBox::ExecLowLevelCommandStateAllowed
		return false;
	}
	return true;
}


/*----- PROTECTED REGION ID(PIBox::PIBoxStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	PIBox::PIBoxStateAllowed.AdditionalMethods

}	//	End of namespace
