/*************************************************************************/
/*! 
 *  \file   BoxController.cpp
 *  \brief  Implementation of BoxController methods.
 *	\author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include <yat/utils/Logging.h>
#include <yat/time/Time.h>

#include <yat4tango/LogHelper.h>

#include "BoxController.h"
#include "SocketManager.h"
#include "Serializer.h"

namespace PIBox_ns
{

BoxController* BoxController::m_instance = NULL;

std::string trim(const std::string& str)
{
    size_t start = str.find_first_not_of(" \t\n\r\f\v");
    if (start == std::string::npos) {
        return "";
    }
    size_t end = str.find_last_not_of(" \t\n\r\f\v");
    return str.substr(start, end - start + 1);
}

std::vector<std::string> splitString(const std::string& str, char delimiter = '\n') {
    std::vector<std::string> tokens;
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

//----------------------------------------------------
// BoxController::BoxController()
//----------------------------------------------------
BoxController::BoxController(yat::String ip_address, int port, yat::StringDictionary dict_socket_config, yat::StringDictionary dict_historic_config, Tango::DeviceImpl* dev)
    :yat4tango::DeviceTask(dev),
    m_exec_low_level_cmd_out_value(""),
    m_model(""),
    m_dict_historic_config(dict_historic_config)
{
    INFO_STREAM << "BoxController::BoxController()" << std::endl;

    try
    {
        SocketManager::instantiate(ip_address, port, dict_socket_config, dev);
        PIBox_ns::SocketManager::get_instance()->write_read("ERR?\n"); // Catch the hardware turning on error
        yat::String commandsList = PIBox_ns::SocketManager::get_instance()->write_read("HLP?\n");
        std::vector<std::string> available_commands_raw = splitString(commandsList);
        for( size_t i=0 ; i<available_commands_raw.size() ; i++)
        {
            const std::string& command_line = available_commands_raw[i];
            size_t pos = command_line.find(" - ");
            if( pos != std::string::npos )
            {
                std::string cmd = trim(command_line.substr(0, pos));
                std::string desc = trim(command_line.substr(pos+3));
                m_available_commands[cmd] = desc;
            }
        }
    }
    catch(const yat::Exception& e)
    {
        set_state(Tango::FAULT);
        set_status("Error while connecting the socket" );
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << e.errors[0].origin << std::endl;
        error_msg << "Desc\t: " << e.errors[0].desc << std::endl;
        error_msg << "Reason\t: " << e.errors[0].reason << std::endl;
        ERROR_STREAM << "Exception from - BoxController::BoxController() :\n"
                    << error_msg.str() << std::endl;
    }

    //fix max size of allSocketMessages attribute 
	unsigned int max_logs = yat::StringUtil::to_num<unsigned int>(m_dict_historic_config.get("max").value_or("100"));	
	m_socket_messages_vect.resize(max_logs);	
	m_socket_messages_vect.clear();

    enable_timeout_msg(false);
    enable_periodic_msg(false);
    set_periodic_msg_period(1000);
    BoxController::m_instance = this;
}

//----------------------------------------------------
// BoxController::~BoxController()
//----------------------------------------------------
BoxController::~BoxController()
{
    INFO_STREAM << "BoxController::~BoxController()" << std::endl;
    SocketManager::reset();
}

BoxController* BoxController::get_instance()
{
    return BoxController::m_instance;
}

bool BoxController::has_command(const std::string& command)
{
    std::map<std::string, std::string>::const_iterator cmd = m_available_commands.find(command);
    return cmd != m_available_commands.cend();
}

//----------------------------------------------------
// BoxController::get_model()
//----------------------------------------------------
yat::String BoxController::get_model()
{
    std::stringstream cmd("*IDN?\n");
    
    yat::Message* msg = yat::Message::allocate(EXEC_READ_MODEL_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(cmd.str());		
    wait_msg_handled(msg, 5000);	
  
    return m_model;
}

//! \brief Get Available Commands
const std::map<std::string, std::string>& BoxController::get_available_commands()
{
    return m_available_commands;
}

//----------------------------------------------------
// BoxController::compute_state_status()
//----------------------------------------------------
void BoxController::compute_state_status()
{
    if (SocketManager::get_instance())    
    {
        set_state(SocketManager::get_instance()->get_state());
        set_status(SocketManager::get_instance()->get_status());
    }
}

//----------------------------------------------------
// BoxController::get_state()
//----------------------------------------------------
Tango::DevState BoxController::get_state()
{
    yat::MutexLock scoped_lock(m_state_status_lock);
    compute_state_status();
    return m_state;
}

//----------------------------------------------------
// BoxController::set_state()
//----------------------------------------------------
void BoxController::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_status_lock);
    m_state = state;
}

//----------------------------------------------------
// BoxController::get_status()
//----------------------------------------------------
yat::String BoxController::get_status()
{
    yat::MutexLock scoped_lock(m_state_status_lock);
    return (m_status_message.str());
}

//----------------------------------------------------
// BoxController::set_status()
//----------------------------------------------------
void BoxController::set_status(const yat::String& status)
{
    yat::MutexLock scoped_lock(m_state_status_lock);
    m_status_message.str("");
    m_status_message << status.c_str();
}

//----------------------------------------------------
// BoxController::exec_low_level_cmd()
//----------------------------------------------------
yat::String BoxController::exec_low_level_cmd(yat::String cmd)
{
    INFO_STREAM << "BoxController::exec_low_level_cmd("<< cmd << ")" << std::endl;
    yat::String out_param("");
  
    std::stringstream cmd_ss("");
    cmd_ss << cmd <<"\n";

    yat::Message* msg = yat::Message::allocate(EXEC_LOW_LEVEL_CMD_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(cmd_ss.str());		
    wait_msg_handled(msg, 5000);	

    out_param = m_exec_low_level_cmd_out_value;
    DEBUG_STREAM << "BoxController::exec_low_level_cmd( " << cmd << " ) => " << out_param << std::endl;
    	
    return out_param;
}

// ============================================================================
// BoxController::compute_socket_messages_logs
// ============================================================================
void BoxController::compute_socket_messages_logs()
{
	DEBUG_STREAM << "BoxController::compute_socket_messages_logs()" << endl;
	try
	{
		std::vector<yat::String> socket_msg_vect = SocketManager::get_instance()->get_socket_msg();   
        for (size_t i = 0; i<socket_msg_vect.size(); i++)
        {
            yat::String socket_msg = socket_msg_vect[i];
            //Remove new line character
            size_t pos_new_line = socket_msg.find_first_of('\n');
            if ( pos_new_line != std::string::npos)
            {
                socket_msg.resize(socket_msg.size () - (socket_msg.size () - pos_new_line));
            }      
            
            if(socket_msg !="")
            {
                yat::String is_timestamp = m_dict_historic_config.get("timestamp").value_or("TRUE");
                std::transform(is_timestamp.begin(), is_timestamp.end(), is_timestamp.begin(), ::toupper);

                std::vector<std::string> parsed_args;
                if(m_socket_messages_vect.size()!=0 && (is_timestamp == "TRUE"))
                {
                    yat::StringUtil::match(m_socket_messages_vect.at(0), "* - *", &parsed_args);
                    if(socket_msg == parsed_args.at(1))
                    {
                        return ;
                    }
                }
                
                yat::CurrentTime t;
                yat::String date_format = m_dict_historic_config.get("format").value_or("%Y-%m-%dT%H:%M:%S");                
                std::stringstream socket_msg_timestamp("");
                if(is_timestamp == "TRUE")
                {
                    socket_msg_timestamp<<t.to_string(date_format, yat::Time::microsec)<<" - "<<socket_msg;	
                }
                else
                {
                    socket_msg_timestamp<<socket_msg;	
                }
                
                m_socket_messages_vect.insert(m_socket_messages_vect.begin(), socket_msg_timestamp.str());		
                unsigned int max_logs = yat::StringUtil::to_num<unsigned int>(m_dict_historic_config.get("max").value_or("100"));
                if(m_socket_messages_vect.size() > max_logs)//in order to keep a max logs only in the attribute 
                {
                    m_socket_messages_vect.erase(m_socket_messages_vect.end()-1);
                }		
            }
        }
	}
    catch (yat::Exception& ex)
    {
        ERROR_STREAM << string(ex.errors[0].desc).c_str() << std::endl;

        std::stringstream error_msg("");
        error_msg << "Origin: " << ex.errors[0].origin << std::endl;
        error_msg << "Desc: " << ex.errors[0].desc << std::endl;
		error_msg << "Reason: " << ex.errors[0].reason << std::endl;

        THROW_DEVFAILED("TANGO_DEVICE_ERROR",
						string(ex.errors[0].desc).c_str(),
						"BoxController::compute_socket_messages_logs");
    }

}

// ============================================================================
// BoxController::get_socket_messages
// ============================================================================
vector<yat::String>& BoxController::get_socket_messages()
{	
    return m_socket_messages_vect;	
}

//----------------------------------------------------
// BoxController::process_message()
//----------------------------------------------------
void BoxController::process_message(yat::Message& msg)
{
	try
	{
		switch(msg.type())
		{
			case yat::TASK_INIT:
			{
				INFO_STREAM << "BoxController::process_message::TASK_INIT" << std::endl;
				enable_periodic_msg(true);
			}
			break;
			
			case EXEC_READ_MODEL_MSG:
            {
				yat::String request = msg.get_data<std::string>();					
                yat::String model_temp = PIBox_ns::SocketManager::get_instance()->write_read(request);
                size_t pos_new_line = model_temp.find_first_of('\n');
                if ( pos_new_line != std::string::npos)
                {
                    model_temp.resize(model_temp.size () - (model_temp.size () - pos_new_line));
                }      
                m_model = model_temp;
			}
			break;
            
			case EXEC_LOW_LEVEL_CMD_MSG:
            {
                INFO_STREAM << "BoxController::process_message::EXEC_LOW_LEVEL_CMD_MSG " << std::endl;
                m_exec_low_level_cmd_out_value = "";
                yat::String cmd = msg.get_data<std::string>();
                yat::String final_cmd = cmd;

				if( cmd.find('?', 0) != std::string::npos )
                {
                    m_exec_low_level_cmd_out_value = PIBox_ns::SocketManager::get_instance()->write_read(final_cmd);
                }
                else if ( cmd.find('#', 0) != std::string::npos )
                {
                    INFO_STREAM << "BoxController::process_message::EXEC_LOW_LEVEL_CMD_MSG -m_model = " << m_model << std::endl;
                    yat::String::size_type pos = m_model.find("SIMULATOR");
                    if(pos !=  std::string::npos)
                    {
                        DEBUG_STREAM << "BoxController::process_message::EXEC_LOW_LEVEL_CMD_MSG - IN SIMULATOR MODE" << std::endl;
                    }
                    else
                    {
                        int code = atoi(cmd.substr(1, cmd.length()-1).str().c_str());
                        final_cmd = code;
                        DEBUG_STREAM << "BoxController::process_message::EXEC_LOW_LEVEL_CMD_MSG - NEW CMD: " << cmd << std::endl;
                    }
                    m_exec_low_level_cmd_out_value = PIBox_ns::SocketManager::get_instance()->write_read(final_cmd);
                }
                else
                {
                    PIBox_ns::SocketManager::get_instance()->write(final_cmd);
                }
                //std::string err_code_low_lvl_cmd = PIBox_ns::SocketManager::get_instance()->write_read("ERR?\n");
                PIAxis_ns::Serializer::error_handler(final_cmd, "low level");
                //DEBUG_STREAM << "BoxController::process_message::EXEC_LOW_LEVEL_CMD_MSG - ERR?: " << err_code_low_lvl_cmd << std::endl;
            }
            break;
            
            case yat::TASK_PERIODIC:
			{
                DEBUG_STREAM << "BoxController::process_message::TASK_PERIODIC" << std::endl;
                compute_socket_messages_logs();
			}
			break;
			
            case yat::TASK_EXIT:
			{
				INFO_STREAM << "BoxController::process_message::TASK_EXIT" << std::endl;
			}
            break;
			
            case yat::TASK_TIMEOUT:
			{
				INFO_STREAM << "BoxController::process_message::TASK_EXIT" << std::endl;
			}
            break;

            default:
            {
				INFO_STREAM << "BoxController::process_message - Unhandled msg type!" << std::endl;
            }
            break;
		}
	}

    catch(Tango::DevFailed& df)
    {
        on_fault(df);
        ERROR_STREAM << "BoxController::process_message - Tango::DevFailed exception: " << yat::String(df.errors[0].desc) << std::endl;    
    }
	catch (const yat::SocketException & se)
	{
		std::stringstream error_msg("");
		error_msg << "Origin\t: " << se.errors[0].origin << std::endl;
	    error_msg << "Desc\t: " << se.errors[0].desc << std::endl;
	    error_msg << "Reason\t: " << se.errors[0].reason << std::endl;

		ERROR_STREAM << "BoxController::process_message - yat::SocketException: " << error_msg.str() << std::endl;
        on_fault(error_msg.str());
	}
	catch (yat::Exception& ex)
    {
        std::stringstream error_msg("");
        error_msg << "Origin: " << ex.errors[0].origin << std::endl;
        error_msg << "Desc: " << ex.errors[0].desc << std::endl;
		error_msg << "Reason: " << ex.errors[0].reason << std::endl;

        ERROR_STREAM << "BoxController::process_message - Exception from SocketManager() : " << error_msg.str() << std::endl;
        on_fault(error_msg.str());
    }
}

// ============================================================================
// BoxController::on_fault(Tango::DevFailed df)
// ============================================================================
void BoxController::on_fault(Tango::DevFailed df)
{
	std::stringstream status;
	status.str("");
    status << "Origin\t: " << df.errors[0].origin << std::endl;
    status << "Desc\t: " << df.errors[0].desc << std::endl;
    status << "Reason\t: " << df.errors[0].reason << std::endl;
    ERROR_STREAM << "BoxController::on_fault(Tango::DevFailed) => " << status.str() << std::endl;

	set_status(status.str());
    set_state(Tango::FAULT);
}

// ============================================================================
// BoxController::on_fault(const yat::String& error_str)
// ============================================================================
void BoxController::on_fault(const yat::String& error_str)
{
    ERROR_STREAM << "BoxController::on_fault(Tango::DevFailed) => " << error_str << std::endl;

	set_state(Tango::FAULT);
    set_status(error_str);
}

} // namespace PIBox_ns
