//*************************************************************************
//! 
// \file BoxController.h
// \author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
// \date 18 October 2022
// \brief class BoxController
// 
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//************************************************************************/


#ifndef BOX_CONTROLLER_H
#define BOX_CONTROLLER_H

#include <tango.h>
#include <yat/utils/String.h>
#include <yat/utils/Dictionary.h>

#include <yat4tango/DeviceTask.h>

#include <sstream>

const size_t EXEC_LOW_LEVEL_CMD_MSG = yat::FIRST_USER_MSG + 100;
const size_t EXEC_READ_MODEL_MSG = yat::FIRST_USER_MSG + 110;

namespace PIBox_ns
{
//--------------------------------------------------------------------------------
//! \class BoxController
//! \brief class Controller
//--------------------------------------------------------------------------------
class BoxController : public yat4tango::DeviceTask
{

public:

    //! \brief Constructor
    //! \param ip_address: yat::String in the format 192.168.0.1
    //! \param port: int that represent the port
    //! \param dict: yat::StringDictionary that contains the Socket options
    BoxController(yat::String ip_address, int port, yat::StringDictionary dict_socket_config, yat::StringDictionary dict_historic_config, Tango::DeviceImpl* dev);
    
    //! \brief Destructor
    ~BoxController();

    //! \brief Get Hardware Model
    yat::String get_model();

    //! \brief Get available commands and description
    const std::map<std::string, std::string>& get_available_commands();

    //! \brief compute state and status of the Box controller
    void compute_state_status();
    
    //! \brief get the state of the Box controller
    Tango::DevState get_state();

    //! \brief set the state of the Box controller
    void set_state(Tango::DevState state);
    
    //! \brief get the status of the Box controller
    yat::String get_status();

    //! \brief set the status of the Box controller
    void set_status(const yat::String& status);

    //! \brief Executes a low level command.
    //! \param  cmd The low level command
    //! \param  outParam Command reply
    yat::String exec_low_level_cmd(yat::String cmd);

    //! \brief get the list of the socket exchanged messages 
    vector<yat::String>& get_socket_messages();

    static BoxController* get_instance();

    bool has_command(const std::string& command);

private:
    //! \brief DeviceTask pure virtual method implementation: the yat messages handler
    //! \param msg Message to handle.
    virtual void process_message(yat::Message& msg);

    //! \brief Manage catch Tango::DevFailed error block
    void on_fault(Tango::DevFailed df);

    //! \brief Manage catch yat::String error block
    void on_fault(const yat::String& error_str);

    //! \brief Compute the socket messages logs
    void compute_socket_messages_logs();

    //! \brief m_state: internal state of PIBox
    Tango::DevState m_state;

    //! \brief m_status_message: internal status of PIBox
    std::stringstream m_status_message;

    //! \brief m_state_lock: Mutex used to protect state and status access
    yat::Mutex m_state_status_lock;

    //! \brief m_exec_low_level_cmd_out_value: Returned request value
    yat::String m_exec_low_level_cmd_out_value;

    //! \brief m_model: Hardware model value
    yat::String m_model;

    //! \brief m_socket_messages_vect: history of socket messages
	std::vector<yat::String> m_socket_messages_vect;

    //! \brief yat::StringDictionary that contains the list of the socket exchanged requests
    yat::StringDictionary m_dict_historic_config;

    //! \brief std::vector<std::string> that contains the list of all available commands on the controller.
    std::map<std::string, std::string> m_available_commands;

    static BoxController* m_instance;
};

} // namespace PIBox_ns

#endif // BOX_CONTROLLER_H