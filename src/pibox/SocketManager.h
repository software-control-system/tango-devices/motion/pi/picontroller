//***********************************************************************
//! 
// \file SocketManager.h
// \author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
// \date 18 October 2022
// \brief class SocketManager
// 
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//***********************************************************************/

#ifndef SOCKET_MANAGER_H
#define SOCKET_MANAGER_H

#include <yat/network/ClientSocket.h>
#include <yat/network/SocketException.h>
#include <yat/memory/UniquePtr.h>
#include <yat/utils/String.h>
#include <yat/utils/Dictionary.h>
#include <yat4tango/YatLogAdapter.h>

namespace PIBox_ns
{

//--------------------------------------------------------------------------------
//! \class SocketManager
//! \brief SocketManager will connect the PIAxis to the Hardware. SocketManager is a Singleton
//--------------------------------------------------------------------------------
class SocketManager : public Tango::LogAdapter
{
public:
    //! \brief Instantiate a SocketManager if there is not an instance yet
    static void instantiate(yat::String ip_address, int port, yat::StringDictionary dict, Tango::DeviceImpl* dev);

    //! \brief Reset the socket manager
    static void reset();
    
    //! \brief Return the instance of the SocketManager if it exist, else return null
    static SocketManager* get_instance();

    //! \brief Connect the socket to a given address and port with configurable options
    //! \param ip_address: yat::String in the format 192.168.0.1
    //! \param port: int that represent the port
    //! \param dict: yat::StringDictionary that contains the Socket options
    void init_socket(yat::String ip_address, int port, yat::StringDictionary dict);

    //! \brief Connect the socket
    void connect();
    
    //! \brief Disconnect the socket
    void disconnect();

    //! \brief Return the socket error status
    int socket_status();

    //! \brief Sets the socket state
    //! \param state The state.
    void set_state(Tango::DevState state);

    //! \brief Gets the state of the socket
    Tango::DevState get_state();

    /// fix the state in a scoped_lock
    void set_status(const std::string& status);

    /// get the last status of the task
    std::string get_status();

    //! \brief Handle the socket error status
    void status_handler();
    
    //! \brief Socket write a buffer
    //! \param buffer: yat::String object
    void write(yat::String buffer);


    //! \brief Socket write a buffer and then read a yat::String
    //! \param buffer: yat::String object
    yat::String write_read(yat::String buffer);

    //static yat::Mutex& get_mutex();

    std::vector<yat::String> get_socket_msg();

private:

    //! \brief Private Constructor
    SocketManager(yat::String ip_address, int port, yat::StringDictionary dict, Tango::DeviceImpl* dev);

    //! \brief Private Destructor
    ~SocketManager();

    //! \brief Socket address
    yat::String m_ip_address;

    //! \brief Socket port
    int m_port;
    
    //! \brief m_socket_manager: Instance of the SocketManager
    static SocketManager* m_socket_manager;

    //! \brief m_socket: Socket object
    yat::ClientSocket m_socket;

    //! \brief m_state: Tango state
    Tango::DevState m_state;

    //! \brief m_status_message: socket state
    std::stringstream m_status;

    //! \brief m_mutex: mutex
    static yat::Mutex m_mutex;

    //! \brief m_state_lock: lock for Tango state
    yat::Mutex m_state_lock;

    //! \brief Mutex to prevent concurrent access
    yat::Mutex m_locker;

    //! \brief log string with the message send/receive
    //yat::String m_socket_msg;
    std::vector<yat::String> m_socket_msg;
    
    yat::Mutex m_socket_msg_locker;
};
    
} // namespace PIBox_ns


#endif // SOCKET_MANAGER_H