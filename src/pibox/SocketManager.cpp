/*************************************************************************/
/*! 
 *  \file   SocketManager.cpp
 *  \brief  Implementation of SocketManager methods.
 *	\author Marc DESGRANGES - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include <tango.h>
#include <yat/utils/Logging.h>

#include "SocketManager.h"
#include <yat4tango/LogHelper.h>

namespace PIBox_ns
{

SocketManager* SocketManager::m_socket_manager = NULL;
yat::Mutex SocketManager::m_mutex;

//----------------------------------------------------
// SocketManager::SocketManager()
//----------------------------------------------------
SocketManager::SocketManager(yat::String ip_address, int port, yat::StringDictionary dict, Tango::DeviceImpl* dev)
    :Tango::LogAdapter(dev),
    m_ip_address(ip_address),
    m_port(port)
{
    INFO_STREAM << "SocketManager::SocketManager()" << std::endl;
    try
    {
        init_socket(m_ip_address, m_port, dict);
        connect();

        set_state(Tango::ON);
        set_status("SOCKET CONNECTED");
    }
    catch (const yat::SocketException & se)
	{
		std::stringstream error_msg("");
	    error_msg << "Origin\t: " << se.errors[0].origin << std::endl;
	    error_msg << "Desc\t: " << se.errors[0].desc << std::endl;
	    error_msg << "Reason\t: " << se.errors[0].reason << std::endl;
        set_state(Tango::FAULT);
        set_status(error_msg.str());

		ERROR_STREAM << "SocketException from - SocketManager() : " << error_msg.str() << std::endl;
	}	
	catch(yat::Exception& ex)
	{
		std::stringstream error_msg("");
	    error_msg << "Origin\t: " << ex.errors[0].origin << std::endl;
	    error_msg << "Desc\t: " << ex.errors[0].desc << std::endl;
	    error_msg << "Reason\t: " << ex.errors[0].reason << std::endl;
        set_state(Tango::FAULT);
        set_status(error_msg.str());

		ERROR_STREAM << "SocketException from - SocketManager() : " << error_msg.str() << std::endl;
	}
}

//----------------------------------------------------
// SocketManager::~SocketManager()
//----------------------------------------------------
SocketManager::~SocketManager()
{
    INFO_STREAM << "SocketManager::~SocketManager()" << std::endl;
    m_socket.disconnect();
    m_socket.terminate();
}

//----------------------------------------------------
// SocketManager::instantiate()
// Instantiate the instance of the singleton
//----------------------------------------------------
void SocketManager::instantiate(yat::String ip_address, int port, yat::StringDictionary dict, Tango::DeviceImpl* dev)
{
    if (m_socket_manager == NULL)
    {
        m_socket_manager = new SocketManager(ip_address, port, dict, dev);
    }
}

//----------------------------------------------------
// SocketManager::reset()
//----------------------------------------------------
void SocketManager::reset()
{
    if (m_socket_manager != NULL)
    {
        delete m_socket_manager;
        m_socket_manager = NULL;
    }
}

//----------------------------------------------------
// SocketManager::get_instance()
// Return the instance of the singleton
//----------------------------------------------------
SocketManager* SocketManager::get_instance()
{
    if (m_socket_manager == NULL)
    {
        Tango::Except::throw_exception("INTERNAL_ERROR",
                        "No instance of SocketManager",
                        "SocketManager::get_instance()");
    }
    return m_socket_manager;
}

//----------------------------------------------------
// SockevstManager::init_socket()
//----------------------------------------------------
void SocketManager::init_socket(yat::String ip_address, int port, yat::StringDictionary dict)
{
    try
    {
        yat::Socket::init();
        
        //set some socket option
        bool is_blocking 	= (dict.get("blocking").value_or("true")=="true") ? true:false;
        bool is_keep_alive = (dict.get("keepalive").value_or("true")=="true") ? true:false;
        bool is_no_delay = (dict.get("nodelay").value_or("true")=="true") ? true:false;
        int snd_tmo = yat::StringUtil::to_num<int>(dict.get("sndtmo").value_or("1000"));
        int rcv_tmo = yat::StringUtil::to_num<int>(dict.get("rcvtmo").value_or("200"));

        m_socket.set_option(yat::Socket::Option::SOCK_OPT_KEEP_ALIVE, is_keep_alive);
        m_socket.set_option(yat::Socket::Option::SOCK_OPT_NO_DELAY, is_no_delay);
        //! Time out period for send operations
        m_socket.set_option(yat::Socket::Option::SOCK_OPT_OTIMEOUT, snd_tmo);
        //! Time out period for receive operations
        m_socket.set_option(yat::Socket::Option::SOCK_OPT_ITIMEOUT, rcv_tmo);

        set_state(Tango::ON);
        set_status("SOCKET CONNECTED");
        INFO_STREAM << "Connected to Server TCP/IP : Addr = "<< ip_address << " Port = "<< port << std::endl;
    }
    catch (const yat::SocketException & se)
	{
		std::stringstream error_msg("");
	    error_msg << "Origin\t: " << se.errors[0].origin << std::endl;
	    error_msg << "Desc\t: " << se.errors[0].desc << std::endl;
	    error_msg << "Reason\t: " << se.errors[0].reason << std::endl;
        set_state(Tango::FAULT);
        set_status(error_msg.str());

		ERROR_STREAM << "SocketException from - SocketManager() : " << error_msg.str() << std::endl;
	}	
	catch(yat::Exception& ex)
	{
		std::stringstream error_msg("");
	    error_msg << "Origin\t: " << ex.errors[0].origin << std::endl;
	    error_msg << "Desc\t: " << ex.errors[0].desc << std::endl;
	    error_msg << "Reason\t: " << ex.errors[0].reason << std::endl;
        set_state(Tango::FAULT);
        set_status(error_msg.str());

		ERROR_STREAM << "SocketException from - SocketManager() : " << error_msg.str() << std::endl;
	}
    
}

//----------------------------------------------------
// SocketManager::connect()
//----------------------------------------------------
void SocketManager::connect()
{
    yat::Address address(m_ip_address, m_port);
    m_socket.connect(address);
}

//----------------------------------------------------
// SocketManager::disconnect()
//----------------------------------------------------
void SocketManager::disconnect()
{
    m_socket.disconnect();
}

//----------------------------------------------------
// SocketManager::socket_status()
//----------------------------------------------------
int SocketManager::socket_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return m_socket.status();
}

//----------------------------------------------------
// SocketManager::set_state()
//----------------------------------------------------
void SocketManager::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

//----------------------------------------------------
// SocketManager::get_state()
//----------------------------------------------------
Tango::DevState SocketManager::get_state()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return m_state;
}

//----------------------------------------------------
// SocketManager::set_status()
//----------------------------------------------------
void SocketManager::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

//----------------------------------------------------
// SocketManager::get_status()
//----------------------------------------------------
std::string SocketManager::get_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return(m_status.str());
}

//----------------------------------------------------
// SocketManager::status_handler()
//----------------------------------------------------
void SocketManager::status_handler()
{
    int status = socket_status();
    if ( status != yat::SocketError::SoErr_NoError)
    {
        set_state(Tango::FAULT);
        set_status(yat::SocketException::get_error_text(status));
        ERROR_STREAM << "SOKET ERROR: " << yat::StringUtil::to_string(status) << " - " << yat::SocketException::get_error_text(status) << std::endl;

        Tango::Except::throw_exception("ERROR",
                                        "Socket Error: " +  yat::StringUtil::to_string(status),
                                        "SocketManager::status_handler()");
    }
}

//----------------------------------------------------
// SocketManager::write()
//----------------------------------------------------
void SocketManager::write(yat::String buffer)
{
    DEBUG_STREAM << "SocketManager::write() - Cmd: " << buffer << std::endl;
    yat::MutexLock scoped_lock(m_locker);
    m_socket_msg.push_back(buffer);
    
    yat::Socket::Data w_data(256, true);
    w_data.memcpy(buffer.c_str(), buffer.size());
    m_socket << w_data;
    status_handler();
}

//----------------------------------------------------
// SocketManager::write_read()
//----------------------------------------------------
yat::String SocketManager::write_read(yat::String buffer)
{
    if( buffer.length()>0 && buffer[0]>=32 && buffer[0]<=126)
    {
        DEBUG_STREAM << "SocketManager::write_read() - Cmd: " << buffer << std::endl;
    }
    else
    {
        std::stringstream debug_msg_in;
        for(size_t i = 0; i < buffer.length(); i++)
        {
            unsigned char val = static_cast<unsigned char>(buffer[i]);
            debug_msg_in << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned int>(val);
            if( i!=buffer.length()-1 ) {
                debug_msg_in << " ";
            }
        }
        DEBUG_STREAM << "SocketManager::write_read() - Cmd(" << buffer.length() << ") hexa: " << debug_msg_in.str() << std::endl;
    }

    yat::MutexLock scoped_lock(m_locker);
    m_socket_msg.push_back(buffer);
   
    yat::Socket::Data w_data(256, true);
    w_data.memcpy(buffer.c_str(), buffer.size());
    m_socket << w_data;
    status_handler();

    yat::Socket::Data w_response_data(1024*8, true);
    std::string response;
    response.reserve(w_response_data.capacity());
    int i=0;
    do
    {
        w_response_data.force_length(0);
        try
        {
            m_socket >> w_response_data;
        }
        catch(yat::SocketException& ex)
        {
            if( response.empty() && ex.errors.size()>0 && ex.errors[0].code!=15 )
            {
                ERROR_STREAM << "SOCKET ERROR(" << ex.code() << ") " << ex.text() << std::endl;
                for( int j=0 ; j<ex.errors.size() ;j++) {
                    ERROR_STREAM << "\tERROR(" << ex.errors[j].code << " " << strerror(ex.errors[j].code)
                                 << ") orig: " << ex.errors[j].origin << " reason: " << ex.errors[j].reason
                                 << " desc: " << ex.errors[j].desc << std::endl;
                }
                throw ex;
            }
        }
        status_handler();
        if( w_response_data.size()>0 )
        {
            response.append(w_response_data.base(), w_response_data.size());
        }
    }
    while (w_response_data.length()>0);
    
    m_socket_msg.push_back(response);

    if( response.length()>0 && response[0]>=32 && response[0]<=126)
    {
        DEBUG_STREAM << "SocketManager::write_read() - Reply: " << response << std::endl;
    }
    else
    {
        std::stringstream debug_msg_out;
        for(size_t i = 0; i < response.length(); i++) {
        unsigned char val = static_cast<unsigned char>(response[i]);
        debug_msg_out << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned int>(val);
        if( i!=response.length()-1 ) {
            debug_msg_out << " ";
        }
        }

        DEBUG_STREAM << "SocketManager::write_read() - Reply(" << response.length() << ") hexa: " << debug_msg_out.str() << std::endl;
    }    
    return response;
}

//----------------------------------------------------
// SocketManager::get_mutex()
//----------------------------------------------------
/*yat::Mutex& SocketManager::get_mutex()
{
    return m_mutex;
}*/

//----------------------------------------------------
// SocketManager::get_log()
//----------------------------------------------------
std::vector<yat::String> SocketManager::get_socket_msg()
{
    yat::MutexLock scoped_lock(m_socket_msg_locker);
    std::vector<yat::String> res = m_socket_msg;
    m_socket_msg.clear();
    return res;
}

} // namespace PIBox_ns
