from conan import ConanFile

class PIControllerRecipe(ConanFile):
    name = "picontroller"
    executable = "ds_PIController"
    version = "1.2.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Salwa Bhaji, Arafat Nourredine, Marc Desgrange"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/pi/picontroller.git"
    description = "PIController motion PI controllers device controller"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/**"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
